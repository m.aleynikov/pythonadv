import time


def decorator_num(num):
    def decorator(func):
        time_start = time.time()
        def wrapper_func(*args, **kwargs):
            i = 0
            while(i < num):
                func()
                i += 1
            time_end = time.time()
            return dict(
                exec_time=round(time_end - time_start, 2),
                name_func=func.__name__
            )
        return wrapper_func
    return decorator

@decorator_num(3)
def do():
    time.sleep(1)

print(do())