class UserStructure:
    __slots__ = ("_name", "_age", "_gender")

    GENDER_MALE = "male"
    GENDER_FEMALE = "female"

    def __init__(self, name, age, gender):
        self.set_name(name)
        self.set_age(age)
        self.set_gender(gender)

    def set_name(self, value):
        if type(value) is not str:
            raise Exception("invalid name")
        self._name = value

    def get_name(self):
        return self._name

    def set_age(self, value):
        if value < 1 or value > 100:
            raise Exception("invalid age")
        self._age = value

    def get_age(self):
        return self._age

    def set_gender(self, value):
        if value not in [self.GENDER_MALE, self.GENDER_FEMALE]:
            raise Exception("invalid gender")
        self._gender = value
    
    def get_gender(self):
        return self._gender

    def __str__(self):
        return f"n:{self._name};a:{self._age};g:{self._gender}"

class Stack:
    _items = []

    def __init__(self, items=None):
        if items is not None:
            for item in items:
                self.add(item)

    def add(self, item):
        self._items.append(item)

    def pop(self):
        return self._items.pop()

    def get_items(self):
        return self._items

user1 = UserStructure("Vova", 22, UserStructure.GENDER_MALE)
user2 = UserStructure("Galya", 18, UserStructure.GENDER_FEMALE)

stack = Stack()
stack.add(user1)
stack.add(user2)
print(stack.get_items())

stack.pop()
print(stack.get_items())

