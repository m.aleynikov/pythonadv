def main():
    countries_dict = {
        "Afghanistan": "Kabul",
        "Albania": "Tirana",
        "Algeria": "Algiers",
        "Andorra": "Andorra la Vella",
        "Angola": "Luanda",
        "Argentina": "Buenos Aires",
        "Armenia": "Yerevan",
        "Australia": "Canberra",
        "Austria": "Vienna",
        "Azerbaijan": "Baku",
    }

    countries_list = [
        "Afghanistan",
        "Algeria",
        "Angola",
        "Armenia",
        "Austria",
    ]

    for country in countries_list:
        if country in countries_dict.keys():
            print(countries_dict[country])

if __name__ == "__main__":
    main()
