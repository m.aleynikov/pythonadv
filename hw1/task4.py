def bank(deposit, period, percent):
    income = (deposit / 100) * percent * period
    return deposit + income

def _input(field):
    value = ""

    while(len(value) == 0):
        print("Enter `{0}`:".format(field), end="")
        value = input()
    return int(value)

def input_deposit():
    return _input('deposit amount')

def input_period():
    return _input('period (ages)')

def input_percent():
    return _input('percent')

def main():
    amount = bank(
        deposit=input_deposit(),
        period=input_period(),
        percent=input_percent()
    )

    print("Amount at the end of deposit is {0}".format(amount))

if __name__ == "__main__":
    main()
