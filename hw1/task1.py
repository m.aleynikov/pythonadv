def main(n, step=1):
    l = list(range(0, n, step))

    for i in l:
        if i > 0 and (i % 2 == 0):
            print("{0}".format(i))

if __name__ == "__main__":
    main(10)
