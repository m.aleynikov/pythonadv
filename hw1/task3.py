def main():
    numbers = list(range(1, 100))

    for n in numbers:
        message = ""

        if n % 3 == 0:
            message += "Fizz"

        if n % 5 == 0:
            message += "Buzz"

        if len(message) > 0:
            print("{0} {1}".format(n, message))

if __name__ == "__main__":
    main()
