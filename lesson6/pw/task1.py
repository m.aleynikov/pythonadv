class CustomList:
    def __init__(self):
        self._list = list()

    def pop(self):
        idx = len(self._list) -1
        value = self._list[idx]
        del self._list[idx]
        return value

    def append(self, value):
        tmp = [0] * (len(self._list) +1)
        for i in range(len(self._list)):
            tmp[i] = self._list[i]
        idx = len(tmp) -1
        tmp[idx] = value
        self._list = tmp

    def remove(self, index):
        try:
            del self._list[index]
        except IndexError:
            pass

    def insert(self, index, value):
        try:
            self._list[index] = value
        except IndexError:
            tmp = [0] * (index +1)
            for i in range(len(self._list)):
                tmp[i] = self._list[i]
            tmp[index] = value
            self._list = tmp

    def clear(self):
        for i in range(len(self._list)):
            del self._list[i]

    def __str__(self):
        return str(self._list)

custom_list = CustomList()

custom_list.append(1)
custom_list.append(2)
custom_list.append(3)

print(custom_list)

value = custom_list.pop()
print(value)
print(custom_list)

custom_list.remove(1)
print(custom_list)

custom_list.insert(4, 5)
print(custom_list)