list_a = [1, 2, 3, 4, 5, 6]

iter_obj = iter(list_a)

# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj)) # StopIteration


class SimpleIterator:
    def __init__(self, start, end, step):
        self._start = start
        self._end = end
        self._step = step

    def __iter__(self):
        return self

    def __next__(self):
        while self._start < self._end:
            self._start += self._step
            return self._start
        raise StopIteration

obj = SimpleIterator(0, 3, 1)
iter_obj = iter(obj)

# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj))
# print(next(iter_obj)) # StopIteration

for o in iter_obj:
    print(o)


obj2 = SimpleIterator(0, 10, 2)
for o in obj2:
    print(o)