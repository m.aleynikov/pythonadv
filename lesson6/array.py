class Array:
    TYPES = (int, str, float)
    def __init__(self, size, default_val=None):
        self._array = [default_val] * size

    def __setitem__(self, key, value):
        if key > len(self) -1:
            raise ValueError("out of range")

        if isinstance(value, Array.TYPES):
            self._array[key] = value

    def __getitem__(self, key):
        if isinstance(key, int) and key <= len(self) -1:
            return self._array[key]
        raise ValueError("out of range")

    def __str__(self):
        return str(self._array)

    def __len__(self):
        return len(self._array)


# array = Array(100)
# array[0] = 12
# print(array[0])
# print(array)

# array[101] = 12

class Array2(list):
    def __init__(self, size, default_val=None):
        elems = [default_val] * size
        super().__init__(elems)

array2 = Array2(100)
array2[0] = 1
print(array2)

print(array2[1:10])