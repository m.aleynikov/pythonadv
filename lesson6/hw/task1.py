import requests
from bs4 import BeautifulSoup
from datetime import datetime
from prettytable import PrettyTable
import sys


FORECAST_URL = 'https://ua.sinoptik.ua/'
SEARCH_CITY_URL = 'https://ua.sinoptik.ua/search.php'

class ForecastDay:
    def __init__(self, date, tmax, tmin):
        self._date = date
        self._tmax = tmax
        self._tmin = tmin

    def get_date(self):
        return self._date

    def get_tmax(self):
        return self._tmax

    def get_tmin(self):
        return self._tmin

    def __str__(self):
        return f"date={self._date};tmax={self._tmax};tmin={self._tmin}"

def extract_date_from_link(link):
    link_parts = link.split('/')
    return datetime.fromisoformat(link_parts[len(link_parts) -1])

def print_table(city, fd_list):
    x = PrettyTable(['day', 'min', 'max'])

    for fd in fd_list:
        date = fd.get_date()
        day = date.strftime('%A %d')
        x.add_row([day, fd.get_tmin(), fd.get_tmax()])
    print(x.get_string(title=f"City: {city}"))

def search_city(q):
    response = requests.get(SEARCH_CITY_URL + f"?q={q}")
    if response.status_code == 200:
        result = response.content.decode('utf-8', 'replace')
        if len(result) > 0:
            row = result.partition('\n')[0]
            parts = row.split('|')
            return dict(
                name=parts[0],
                uri=parts[2] 
            )
    return None

def show_forecast(city):
    response = requests.get(FORECAST_URL + f"/{city['uri']}")

    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        block_days = soup.find(id='blockDays')

        fd_list = []
        for day_tab in block_days.find_all(class_='main'):
            day_link = day_tab.find(class_='day-link')
            date = extract_date_from_link(day_link['data-link'])
            tmax = day_tab.find('div', class_='max').find('span')
            tmin = day_tab.find('div', class_='min').find('span')

            fd = ForecastDay(
                date=date,
                tmax=tmax.get_text(),
                tmin=tmin.get_text(),
            )
            fd_list.append(fd)

        print_table(city['name'], fd_list)

def main():
    if len(sys.argv) > 1:
        city = search_city(sys.argv[1])
        if city is None:
            print('City not found')
        else:
            show_forecast(city)
    else:
        print('please provide a city')

if __name__ == '__main__':
    main()
