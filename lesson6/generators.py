def generator_func(start, end, step):
    while start < end:
        start += step
        yield start
        yield "I am generator func"
    raise StopIteration

obj = generator_func(0, 3, 1)
iter_obj = iter(obj)

print(next(iter_obj))
print(next(iter_obj))
print(next(iter_obj))
# print(next(iter_obj)) # StopIteration

generator_expression = (x ** 2 for x in range(100)) # <generator object <genexpr> at 0x105a474d0>
print(generator_expression)

obj2 = generator_func(1, 3, 1)
iter_obj2 = iter(obj2)

print(next(iter_obj2))
print(next(iter_obj2))
print(next(iter_obj2))
