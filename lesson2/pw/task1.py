class VehicleError(Exception):
    """Raised when the something invalid in our logic"""
    pass

class VehicleEngineType:
    PETROL="petrol"
    DIESEL="diesel"
    ELECTRIC="electric"

class Vehicle:
    __slots__ = ("_name", "_brand", "_engine_type")

    brand_list = []

    def __init__(self):
        pass

    def set_name(self, value):
        self._name = value

    def get_name(self):
        return self._name

    def set_brand(self, value):
        if value not in self.brand_list:
            raise VehicleError(f"Brand `{value}` doesnot avaliable")
        self._brand = value

    def get_brand(self):
        self._brand

    def set_engine_type(self, value):
        if value not in [VehicleEngineType.PETROL, VehicleEngineType.DIESEL, VehicleEngineType.ELECTRIC]:
            raise ValueError("Invalid engine type value")
        self._engine_type = value

    def get_engine_type(self):
        return self._engine_type

    def go(self):
        raise VehicleError("This method not implemented yet")

    @property
    def about(self):
        return dict(
            name=self._name,
            brand=self._brand,
            engine=self._engine_type
        )

class Car(Vehicle):
    brand_list = ["BMW", "Volkswagen", "Seat"]

    def __init__(self, name, brand, engine_type=VehicleEngineType.PETROL):
        self.set_name(name)
        self.set_brand(brand)
        self.set_engine_type(engine_type)

    def go(self):
        print(f"Car `{self._name}` is moving...")

class Truck(Vehicle):
    brand_list = ["Volvo", "Man", "Daf"]

    def __init__(self, name, brand, engine_type=VehicleEngineType.DIESEL):
        self.set_name(name)
        self.set_brand(brand)
        self.set_engine_type(engine_type)

    def go(self):
        print(f"Truck `{self._name}` is moving...")

def main():
    car = Car("baby car", "BMW")
    car.go()
    print(car.about)

    truck = Truck("dady car", "Volvo")
    truck.go()
    print(truck.about)


if __name__ == "__main__":
    main()
