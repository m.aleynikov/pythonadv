class Store:
    countof_products=0

    def __init__(self, name, countof_products=0):
        self._name = name
        self._countof_products = countof_products

    def get_name(self):
        return self._name
    
    def set_name(self, value):
        self._name = value

    def set_countof_products(self, value):
        self._countof_products = value

    def get_countof_products(self):
        return self._countof_products

    def increment_product(self, count=1):
        self._countof_products += count
        Store.countof_products += count

    @staticmethod
    def get_countof_all_products():
        return Store.countof_products


def main():
    store1 = Store("Rozetka")
    store2 = Store("Comfy")
    store3 = Store("Foxtrot")

    store1.increment_product()

    store2.increment_product()
    store2.increment_product()

    store3.increment_product()
    store3.increment_product()
    store3.increment_product()

    print(f"Count of all sold products: {Store.get_countof_all_products()}")

if __name__ == "__main__":
    main()
