class Point:
    def __init__(self, x, y, z):
        self._x = x
        self._y = y
        self._z = z

    def get_x(self):
        return self._x

    def set_x(self, value):
        self._x = value

    def get_y(self):
        return self._y

    def set_y(self, value):
        self._y = value

    def get_z(self):
        return self._z

    def set_z(self, value):
        self._z = value

    def __add__(self, other):
        x_new = self.get_x() + other.get_x()
        y_new = self.get_y() + other.get_y()
        z_new = self.get_z() + other.get_z()

        return Point(x_new, y_new, z_new)

    def __sub__(self, other):
        x_new = self.get_x() - other.get_x()
        y_new = self.get_y() - other.get_y()
        z_new = self.get_z() - other.get_z()

        return Point(x_new, y_new, z_new)

    def __mul__(self, other):
        x_new = self.get_x() * other.get_x()
        y_new = self.get_y() * other.get_y()
        z_new = self.get_z() * other.get_z()

        return Point(x_new, y_new, z_new)

    def __div__(self, other):
        x_new = self.get_x() / other.get_x()
        y_new = self.get_y() / other.get_y()
        z_new = self.get_z() / other.get_z()

        return Point(x_new, y_new, z_new)

    def __str__(self):
        return f"x: {self._x}; y: {self._y}; z: {self._z}"

def main():
    point1 = Point(1, 2, 3)
    point2 = Point(1, 2, 3)

    result = point1 * point2
    print(result)


if __name__ == "__main__":
    main()
